var obj = {
    options: {
        markedOptions: {
            breaks: true,
        },
        upload: true,
        uploadOptions: {
            url: '/system/user/upload/file',
            multiple: true,
            name: 'Upload[file]',
        },
        uploadPlaceholder: '',
        label: {
            drop: 'Перетащите файл или',
            select: 'выберите его',
            preview: 'Предварительный просмотр',
            text: 'Текст',
            markdown: 'Поддерживается Markdown'
        },
        height: 150
    },
    init: function (options, element) {
        this.element = $(element)
        this.options = $.extend(true, this.options, options)
        this._renderSwitcher()
        this.marked = marked

        if (this.options.upload) {
            this._renderUpload()
        }

        return this
    },
    _upload: function(bar) {
        var _this = this
        var defaultOptions = {
            error: function() {
                console.log('error', arguments);
            },
            loadStart: function(e) {
                bar.removeAttribute('hidden');
                bar.max = e.total;
                bar.value = e.loaded;
            },
            progress: function(e) {
                bar.max = e.total;
                bar.value = e.loaded;
            },
            loadEnd: function(e) {
                bar.max = e.total;
                bar.value = e.loaded;
            },
            complete: function(data) {
                _this._insertFileMD(data.response)
            },
            completeAll: function() {
                setTimeout(function () {
                    bar.setAttribute('hidden', 'hidden');
                }, 1000);
            }
        }
        var options = $.extend({}, defaultOptions, this.options.uploadOptions)
        var placeholder = this.placeholder

        if (this.options.uploadPlaceholder) {
            var parentElement = this.editor.closest(this.options.uploadPlaceholder)
            if (parentElement.length) {
                placeholder = parentElement
            }
        }
        UIkit.upload(placeholder, options);
    },
    _insertFileMD: function(data) {
        var _this = this
        this.help.empty()

        if (data) {
            var value = ''
            data = $.parseJSON(data)

            if (data.errors) {
                $.each(data.errors, function(index, error) {
                    _this.help.append('<div class="uk-alert-danger" uk-alert><a class="uk-alert-close" uk-close></a><p>' + error + '</p></div>')
                })
            } else {
                value = '[' + data.name + '](' + data.url + ')'
                if (data.type.split('/')[0] === 'image') {
                    value = '!' + value
                }
            }
            var textbox = this.textbox.get(0)

            if (textbox.value) {
                value = '\n' + value
            }
            var caretPos = textbox.selectionStart
            var textAreaTxt = textbox.value
            value = textAreaTxt.substring(0, caretPos) + value + textAreaTxt.substring(caretPos)
            this.element.val(value).change()
        }
    },
    _renderUpload: function() {
        var progress = $('<progress class="uk-progress" value="0" max="100" hidden></progress>')
        var link = $([
            '<div>',
                '<span uk-icon="icon: cloud-upload"></span>',
                '<span class="uk-text-middle">' + this.options.label.drop + '</span>',
                '<div uk-form-custom>',
                    '<input type="file" multiple>',
                    '<span class="uk-link">' + this.options.label.select + '</span>',
                '</div>',
            '</div>',
        ].join(' '))
        var supported = $([
            '<div>',
                '<a class="uk-text-small uk-link-muted" href="https://guides.github.com/features/mastering-markdown/#examples" target="_blank" rel="noopener noreferrer"><i class="fab fa-markdown"></i> ' + this.options.label.markdown + '</a>',
            '</div>',
        ].join(' '))
        var grid = $('<div class="uk-text-small uk-margin-small-top uk-grid uk-flex uk-flex-between uk-flex-middle" uk-grid>').append([link, supported])
        this.help = $('<div>')
        this.placeholder.parent().append([grid, progress, this.help])

        this._upload(progress.get(0))
    },
    _renderSwitcher: function() {
        var _this = this
        this.editor = $('<div class="ex-markdown-editor">')
        this.element.prop('hidden', true).before(this.editor)
        this.placeholder = $('<div>')
        this.mdPreview = $('<li class="markdown">')

        var switcher = $('<ul class="uk-switcher uk-margin-top">')
            .append([$('<li class="uk-position-relative">').append(this.placeholder), this.mdPreview])

        var tab = $('<ul uk-tab>')
        var mdTabLink = $('<li><a href="#">' + this.options.label.preview + '</a></li>')
        tab.append(['<li><a href="#">' + this.options.label.text + '</a></li>', mdTabLink])

        this.editor.append([tab, switcher])
        this.textbox = $('<textarea class="uk-placeholder uk-textarea">')
            .css('min-height', this.options.height)
            .prependTo(this.placeholder)
            .on('input', function() {
                _this.element.val($(this).val())
            })
            .val(this.element.val())

        mdTabLink.on('click', function() {
            var value = _this.element.val()
            _this.mdPreview
                .html(_this.marked(value, _this.options.markedOptions))
                .css('min-height', _this.placeholder.parent().height())
        })

        this.element.on('change', function() {
            _this.textbox.val(this.value)

            if (!this.value) {
                UIkit.switcher(tab).show(0)
            }
        })
    },
    destroy: function() {
        this.element.removeAttr('hidden ex-markdown-editor')
        $(this).removeData('markdownEditor')
        this.editor.remove()
    }
};

if ( typeof Object.create !== "function" ) {
    Object.create = function (o) {
        function F() {}
        F.prototype = o
        return new F()
    };
}
$.plugin = function( name, object ) {
    $.fn[name] = function( options ) {
        return this.each(function() {
            if ( ! $.data( this, name ) ) {
                $.data( this, name, Object.create(object).init(
                    options, this ) )
                }
            });
        };
    };
$.plugin('markdownEditor', obj)
