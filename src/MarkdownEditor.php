<?php

namespace exoo\markdowneditor;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * Markdown editor
 *
 * For example to use the Markdown editor with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo MarkdownEditor::widget([
 *     'model' => $model,
 *     'attribute' => 'text',
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo MarkdownEditor::widget([
 *     'name'  => 'text',
 *     'value'  => $value,
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'text')->widget(MarkdownEditor::class, [
 *     'clientOptions' => [],
 * ]) ?>
 * ```
 * @since 1.0
 */
class MarkdownEditor extends InputWidget
{
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];

    /**
     * @inheritdoc
     */
    public function run()
	{
        $options = Json::htmlEncode($this->clientOptions);
        $id = $this->options['id'];
        $view = $this->getView();
        $view->registerJs("jQuery('#$id').markdownEditor($options);");
        MarkdownEditorAsset::register($view);

        if ($this->hasModel()) {
            return Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            return Html::textarea($this->name, $this->value, $this->options);
        }
	}
}
