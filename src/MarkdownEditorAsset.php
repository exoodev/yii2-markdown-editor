<?php

namespace exoo\markdowneditor;

/**
 * Asset bundle for widget [[MarkdownEditor]].
 */
class MarkdownEditorAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/markdowneditor/assets';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/markdown-editor.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'js/markdown-editor.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'exoo\marked\MarkedAsset',
        'exoo\fontawesome\FontAwesomeAsset',
    ];
}
